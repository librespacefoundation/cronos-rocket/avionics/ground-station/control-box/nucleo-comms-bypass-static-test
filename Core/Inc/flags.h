/*
 * loadCell.h
 *
 *  Created on: Jan 27, 2022
 *      Author: thodoris
 */

#ifndef INC_FLAGS_H_
#define INC_FLAGS_H_

#include "stdint.h"

typedef struct {
	volatile uint8_t launchActivated;
	volatile uint8_t fillActivated;
	volatile uint8_t fireActivated;
	volatile uint8_t approachActivated;
	volatile uint8_t purgeActivated;

	volatile uint8_t kg_0;
	volatile uint8_t kg_1_7;
	volatile uint8_t kg_3_3;
}Flags;


void initializeFlags(Flags *flags);

#endif /* INC_FLAGS_H_ */
