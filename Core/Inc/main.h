/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define B1_EXTI_IRQn EXTI15_10_IRQn
#define LD_FIRE_Pin GPIO_PIN_1
#define LD_FIRE_GPIO_Port GPIOC
#define LD_FILL_Pin GPIO_PIN_0
#define LD_FILL_GPIO_Port GPIOA
#define LD_VALVE_CHECK_Pin GPIO_PIN_1
#define LD_VALVE_CHECK_GPIO_Port GPIOA
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define LD_NEUTRAL_Pin GPIO_PIN_4
#define LD_NEUTRAL_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define LD_APPROACH_Pin GPIO_PIN_1
#define LD_APPROACH_GPIO_Port GPIOB
#define LD_FLIGHT_Pin GPIO_PIN_10
#define LD_FLIGHT_GPIO_Port GPIOB
#define CS_SD_Pin GPIO_PIN_14
#define CS_SD_GPIO_Port GPIOB
#define GPIO_EXTI7_PARACHUTES_Pin GPIO_PIN_7
#define GPIO_EXTI7_PARACHUTES_GPIO_Port GPIOC
#define GPIO_EXTI7_PARACHUTES_EXTI_IRQn EXTI9_5_IRQn
#define GPIO_EXTI9_ABORT_Pin GPIO_PIN_9
#define GPIO_EXTI9_ABORT_GPIO_Port GPIOC
#define GPIO_EXTI9_ABORT_EXTI_IRQn EXTI9_5_IRQn
#define GPIO_EXTI8_CALIBRATE_Pin GPIO_PIN_8
#define GPIO_EXTI8_CALIBRATE_GPIO_Port GPIOA
#define GPIO_EXTI8_CALIBRATE_EXTI_IRQn EXTI9_5_IRQn
#define LD_READY_Pin GPIO_PIN_10
#define LD_READY_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define GPIO_EXTI10_LAUNCH_Pin GPIO_PIN_10
#define GPIO_EXTI10_LAUNCH_GPIO_Port GPIOC
#define GPIO_EXTI10_LAUNCH_EXTI_IRQn EXTI15_10_IRQn
#define GPIO_EXTI11_DROGUE_Pin GPIO_PIN_11
#define GPIO_EXTI11_DROGUE_GPIO_Port GPIOC
#define GPIO_EXTI11_DROGUE_EXTI_IRQn EXTI15_10_IRQn
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
