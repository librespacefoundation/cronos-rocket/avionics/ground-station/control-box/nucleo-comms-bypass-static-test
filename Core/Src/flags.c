/*
 * flags.c
 *
 *  Created on: Jan 27, 2022
 *      Author: thodoris
 */

#include "flags.h"

void initializeFlags(Flags *flags) {
	flags->launchActivated = 0;
	flags->fillActivated = 0;
	flags->fireActivated = 0;
	flags->approachActivated = 0;

	flags->purgeActivated = 0;
}
