/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "flags.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
// CAN StdId
#define GLOBAL_ID 					0x0
#define DATA_LOGGER_ID 				0x0447
#define TELEMETRY_BOARD_ID 			0x0448
#define MOTOR_CONTROLLER_BOARD_ID 	0x0449

#define LAUNCH_SWITCH_ID 	0
#define ABORT_SWITCH_ID 	4

#define RUNTANK_WEIGHT_ZERO_ID 	1
#define RUNTANK_WEIGHT_1_7_ID 	2
#define RUNTANK_WEIGHT_3_3_ID 	3

// CAN ExtId
#define CAN_TX_FRAME_1				0
#define CAN_TX_FRAME_2				1


enum States{
	NEUTRAL_STATE,
	VAVLE_CHECK_STATE,
	FILL_STATE,
	FIRE_STATE,
	APPROACH_STATE,
	ABORT_STATE,
	READY_STATE,
};
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
static Flags flags;

uint8_t uart_buffer[20000];
uint8_t uart_buffer2[20000];
uint8_t chose = 0;

uint8_t uart1_rxBuffer[1];

int size1, size2;
int counter1, counter2;

CAN_TxHeaderTypeDef   TxHeader;
CAN_RxHeaderTypeDef   RxHeader;
uint8_t               RxData[8];

uint32_t TxMailbox;

enum States progress;
uint8_t prev_state = -1;
uint8_t start = 0;
uint8_t flag = 0;

uint8_t kg_counter = 0;
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef hcan1;

I2C_HandleTypeDef hi2c1;

SPI_HandleTypeDef hspi2;

UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart2_tx;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_CAN1_Init(void);
static void MX_SPI2_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */


static void toggle_LEDS() {
	switch(progress) {
	  case NEUTRAL_STATE:
		  HAL_GPIO_WritePin(LD_NEUTRAL_GPIO_Port, LD_NEUTRAL_Pin, SET);
		  HAL_GPIO_WritePin(LD_VALVE_CHECK_GPIO_Port, LD_VALVE_CHECK_Pin, RESET);
		  HAL_GPIO_WritePin(LD_FILL_GPIO_Port, LD_FILL_Pin, RESET);
		  HAL_GPIO_WritePin(LD_FIRE_GPIO_Port, LD_FIRE_Pin, RESET);
		  HAL_GPIO_WritePin(LD_APPROACH_GPIO_Port, LD_APPROACH_Pin, RESET);

		  break;
	  case VAVLE_CHECK_STATE:
		  HAL_GPIO_WritePin(LD_NEUTRAL_GPIO_Port, LD_NEUTRAL_Pin, RESET);
		  HAL_GPIO_WritePin(LD_VALVE_CHECK_GPIO_Port, LD_VALVE_CHECK_Pin, SET);
		  HAL_GPIO_WritePin(LD_FILL_GPIO_Port, LD_FILL_Pin, RESET);
		  HAL_GPIO_WritePin(LD_FIRE_GPIO_Port, LD_FIRE_Pin, RESET);
		  HAL_GPIO_WritePin(LD_APPROACH_GPIO_Port, LD_APPROACH_Pin, RESET);
		  break;
	  case FILL_STATE:
		  HAL_GPIO_WritePin(LD_NEUTRAL_GPIO_Port, LD_NEUTRAL_Pin, RESET);
		  HAL_GPIO_WritePin(LD_VALVE_CHECK_GPIO_Port, LD_VALVE_CHECK_Pin, RESET);
		  HAL_GPIO_WritePin(LD_FILL_GPIO_Port, LD_FILL_Pin, SET);
		  HAL_GPIO_WritePin(LD_FIRE_GPIO_Port, LD_FIRE_Pin, RESET);
		  HAL_GPIO_WritePin(LD_APPROACH_GPIO_Port, LD_APPROACH_Pin, RESET);
		  break;
	  case FIRE_STATE:
		  HAL_GPIO_WritePin(LD_NEUTRAL_GPIO_Port, LD_NEUTRAL_Pin, RESET);
		  HAL_GPIO_WritePin(LD_VALVE_CHECK_GPIO_Port, LD_VALVE_CHECK_Pin, RESET);
		  HAL_GPIO_WritePin(LD_FILL_GPIO_Port, LD_FILL_Pin, RESET);
		  HAL_GPIO_WritePin(LD_FIRE_GPIO_Port, LD_FIRE_Pin, SET);
		  HAL_GPIO_WritePin(LD_APPROACH_GPIO_Port, LD_APPROACH_Pin, RESET);
		  break;
	  case APPROACH_STATE:
		  HAL_GPIO_WritePin(LD_NEUTRAL_GPIO_Port, LD_NEUTRAL_Pin, RESET);
		  HAL_GPIO_WritePin(LD_VALVE_CHECK_GPIO_Port, LD_VALVE_CHECK_Pin, RESET);
		  HAL_GPIO_WritePin(LD_FILL_GPIO_Port, LD_FILL_Pin, RESET);
		  HAL_GPIO_WritePin(LD_FIRE_GPIO_Port, LD_FIRE_Pin, RESET);
		  HAL_GPIO_WritePin(LD_APPROACH_GPIO_Port, LD_APPROACH_Pin, SET);
		  break;
	  case ABORT_STATE:
		  HAL_GPIO_WritePin(LD_NEUTRAL_GPIO_Port, LD_NEUTRAL_Pin, SET);
		  HAL_GPIO_WritePin(LD_VALVE_CHECK_GPIO_Port, LD_VALVE_CHECK_Pin, SET);
		  HAL_GPIO_WritePin(LD_FILL_GPIO_Port, LD_FILL_Pin, SET);
		  HAL_GPIO_WritePin(LD_FIRE_GPIO_Port, LD_FIRE_Pin, SET);
		  HAL_GPIO_WritePin(LD_APPROACH_GPIO_Port, LD_APPROACH_Pin, SET);
		  break;
	  case READY_STATE:
		  HAL_GPIO_WritePin(LD_NEUTRAL_GPIO_Port, LD_NEUTRAL_Pin, RESET);
		  HAL_GPIO_WritePin(LD_VALVE_CHECK_GPIO_Port, LD_VALVE_CHECK_Pin, RESET);
		  HAL_GPIO_WritePin(LD_FILL_GPIO_Port, LD_FILL_Pin, RESET);
		  HAL_GPIO_WritePin(LD_FIRE_GPIO_Port, LD_FIRE_Pin, RESET);
		  HAL_GPIO_WritePin(LD_APPROACH_GPIO_Port, LD_APPROACH_Pin, RESET);
		  break;
	  default:
		  break;
	  }
	  prev_state = progress;
}

static void canSetup(void)
{
	  CAN_FilterTypeDef canfilterconfig;

	  canfilterconfig.FilterActivation = CAN_FILTER_ENABLE;
	  canfilterconfig.FilterBank = 0;  // which filter bank to use from the assigned ones
	  canfilterconfig.FilterFIFOAssignment = CAN_FILTER_FIFO0;
	  canfilterconfig.FilterIdHigh = 0;
	  canfilterconfig.FilterIdLow = 0;
	  canfilterconfig.FilterMaskIdHigh = 0;
	  canfilterconfig.FilterMaskIdLow = 0;
	  canfilterconfig.FilterMode = CAN_FILTERMODE_IDMASK;
	  canfilterconfig.FilterScale = CAN_FILTERSCALE_32BIT;
//	  canfilterconfig.SlaveStartFilterBank = 20;  // how many filters to assign to the CAN1 (master can)

	if (HAL_CAN_ConfigFilter(&hcan1, &canfilterconfig) != HAL_OK) {
		/* Filter configuration Error */
		Error_Handler();
	}

	if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK) {
		/* Notification Error */
		Error_Handler();
	}

	if (HAL_CAN_Start(&hcan1) != HAL_OK) {
		/* Start Error */
		Error_Handler();
	}
}

int8_t canSendFlags(){
	if (!(flags.launchActivated ||
			flags.fillActivated ||
			flags.fireActivated ||
			flags.approachActivated ||
			flags.kg_0 || flags.kg_1_7 || flags.kg_3_3 ||
			flags.purgeActivated)) {
		return 1;
	}

	CAN_TxHeaderTypeDef   TxHeader;
	uint8_t               TxData[8];
	static uint32_t              TxMailbox;

	TxHeader.IDE = CAN_ID_EXT;
	TxHeader.RTR = CAN_RTR_DATA;
	TxHeader.ExtId = (MOTOR_CONTROLLER_BOARD_ID << 4);
	TxHeader.DLC = 1;

	if (flags.purgeActivated == 1) {
		TxHeader.ExtId |= ABORT_SWITCH_ID;
		flags.purgeActivated = 0;
		TxData[0] = '5';
	}
	else if (flags.launchActivated == 1) {
		TxHeader.ExtId |= LAUNCH_SWITCH_ID;
		flags.launchActivated = 0;
		TxData[0] = '1';
	}
	else if (flags.kg_0 == 1) {
		TxHeader.ExtId |= RUNTANK_WEIGHT_ZERO_ID;
		flags.kg_0 = 0;
		TxData[0] = '1';
	}
	else if (flags.kg_1_7 == 1) {
		TxHeader.ExtId |= RUNTANK_WEIGHT_1_7_ID;
		flags.kg_1_7 = 0;
		TxData[0] = '1';
	}
	else if (flags.kg_3_3 == 1) {
		TxHeader.ExtId |= RUNTANK_WEIGHT_3_3_ID;
		flags.kg_3_3 = 0;
		TxData[0] = '1';
	}

	if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, &TxMailbox) != HAL_OK)
	{
	   Error_Handler ();
		return -1;
	}
	while (HAL_CAN_IsTxMessagePending(&hcan1, TxMailbox)) {}

	return 0;
}

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_CAN1_Init();
  MX_SPI2_Init();
  MX_USART3_UART_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */

  canSetup();
  initializeFlags(&flags);

  // CLOSE
  HAL_Delay(1000);

  HAL_GPIO_TogglePin(LD_NEUTRAL_GPIO_Port, LD_NEUTRAL_Pin);
  HAL_Delay(1000);
  HAL_GPIO_TogglePin(LD_VALVE_CHECK_GPIO_Port, LD_VALVE_CHECK_Pin);
  HAL_Delay(1000);
  HAL_GPIO_TogglePin(LD_FILL_GPIO_Port, LD_FILL_Pin);
  HAL_Delay(1000);
  HAL_GPIO_TogglePin(LD_FIRE_GPIO_Port, LD_FIRE_Pin);
  HAL_Delay(1000);
  HAL_GPIO_TogglePin(LD_APPROACH_GPIO_Port, LD_APPROACH_Pin);
  HAL_Delay(1000);

  // OPEN
//  LD_APPROACH
  HAL_GPIO_TogglePin(LD_NEUTRAL_GPIO_Port, LD_NEUTRAL_Pin);
  HAL_GPIO_TogglePin(LD_VALVE_CHECK_GPIO_Port, LD_VALVE_CHECK_Pin);
  HAL_GPIO_TogglePin(LD_FILL_GPIO_Port, LD_FILL_Pin);
  HAL_GPIO_TogglePin(LD_FIRE_GPIO_Port, LD_FIRE_Pin);
  HAL_GPIO_TogglePin(LD_APPROACH_GPIO_Port, LD_APPROACH_Pin);

  HAL_Delay(1000);

  HAL_GPIO_TogglePin(LD_NEUTRAL_GPIO_Port, LD_NEUTRAL_Pin);
  HAL_GPIO_TogglePin(LD_VALVE_CHECK_GPIO_Port, LD_VALVE_CHECK_Pin);
  HAL_GPIO_TogglePin(LD_FILL_GPIO_Port, LD_FILL_Pin);
  HAL_GPIO_TogglePin(LD_FIRE_GPIO_Port, LD_FIRE_Pin);
  HAL_GPIO_TogglePin(LD_APPROACH_GPIO_Port, LD_APPROACH_Pin);

  start = 1;
//  HAL_DMA_Start(&hdma_usart2_tx,  huart2.Instance->TDR,	uart_buffer, 20000);
  uint32_t timer = HAL_GetTick();
  HAL_DMA_Init(&hdma_usart2_tx);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  if (HAL_GetTick() - timer > 100) {
		  if (size1 > 50) {
			  chose = !chose;
			  size1 += sprintf((char *)(uart_buffer + size1), "\r\n");
			  HAL_UART_Transmit_DMA(&huart2, uart_buffer, size1);
			  size1 = 0;

		  }
		  else if (size2 > 50) {
			  chose = !chose;
			  size2 += sprintf((char *)(uart_buffer2 + size2), "\r\n");
			  HAL_UART_Transmit_DMA(&huart2, uart_buffer2, size2);
			  size2 = 0;
		  }
		  timer = HAL_GetTick();
	  }
	  // transmit as soon as possible (if needed)
	  canSendFlags();

//	  HAL_Delay(50);
	  if (prev_state != progress) {
		  toggle_LEDS();
	  }
  }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief CAN1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN1_Init(void)
{

  /* USER CODE BEGIN CAN1_Init 0 */

  /* USER CODE END CAN1_Init 0 */

  /* USER CODE BEGIN CAN1_Init 1 */

  /* USER CODE END CAN1_Init 1 */
  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 16;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan1.Init.TimeSeg1 = CAN_BS1_12TQ;
  hcan1.Init.TimeSeg2 = CAN_BS2_7TQ;
  hcan1.Init.TimeTriggeredMode = DISABLE;
  hcan1.Init.AutoBusOff = DISABLE;
  hcan1.Init.AutoWakeUp = DISABLE;
  hcan1.Init.AutoRetransmission = DISABLE;
  hcan1.Init.ReceiveFifoLocked = DISABLE;
  hcan1.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN1_Init 2 */

  /* USER CODE END CAN1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x10909CEC;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_4BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 1000000;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD_FIRE_GPIO_Port, LD_FIRE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LD_FILL_Pin|LD_VALVE_CHECK_Pin|LD_NEUTRAL_Pin|LD2_Pin
                          |LD_READY_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0|LD_APPROACH_Pin|GPIO_PIN_2|LD_FLIGHT_Pin
                          |CS_SD_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD_FIRE_Pin */
  GPIO_InitStruct.Pin = LD_FIRE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD_FIRE_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD_FILL_Pin LD_VALVE_CHECK_Pin LD_NEUTRAL_Pin LD2_Pin
                           LD_READY_Pin */
  GPIO_InitStruct.Pin = LD_FILL_Pin|LD_VALVE_CHECK_Pin|LD_NEUTRAL_Pin|LD2_Pin
                          |LD_READY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB0 LD_APPROACH_Pin PB2 LD_FLIGHT_Pin
                           CS_SD_Pin */
  GPIO_InitStruct.Pin = GPIO_PIN_0|LD_APPROACH_Pin|GPIO_PIN_2|LD_FLIGHT_Pin
                          |CS_SD_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : GPIO_EXTI7_PARACHUTES_Pin GPIO_EXTI9_ABORT_Pin GPIO_EXTI10_LAUNCH_Pin GPIO_EXTI11_DROGUE_Pin */
  GPIO_InitStruct.Pin = GPIO_EXTI7_PARACHUTES_Pin|GPIO_EXTI9_ABORT_Pin|GPIO_EXTI10_LAUNCH_Pin|GPIO_EXTI11_DROGUE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : GPIO_EXTI8_CALIBRATE_Pin */
  GPIO_InitStruct.Pin = GPIO_EXTI8_CALIBRATE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIO_EXTI8_CALIBRATE_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */

/*
 * based on: https://gitlab.com/librespacefoundation/cronos-rocket/avionics/motorcontroller/motorcontroller-sw/-/issues/39
 */
void HAL_CAN_RxFifo0MsgPendingCallback (CAN_HandleTypeDef *hcan)
{
  CAN_RxHeaderTypeDef RxHeader;

  static uint8_t RxData[8];

  /* Get RX message */
  if (HAL_CAN_GetRxMessage (hcan, CAN_RX_FIFO0, &RxHeader, (uint8_t*)&RxData) != HAL_OK) {
    /* Reception Error */
    Error_Handler();
  }

  if (start != 1)
	  return ;

  RxHeader.ExtId = RxHeader.StdId & 0xf;
  RxHeader.StdId = (RxHeader.StdId - (RxHeader.ExtId & 0xf)) >> 4;
//  HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
  if(RxHeader.StdId == GLOBAL_ID){
//	  HAL_GPIO_TogglePin(LED_RX_GPIO_Port, LED_RX_Pin);
	  switch(RxHeader.ExtId) {
	  case CAN_TX_FRAME_1:
	  {
		  /*
		  	 * 1st data: 		1		  2			3		4					5,6							7,8
		  	 * 				 engine1 | engine2 | engine3 | pressure rt 2 float points | pres injector 2 float points |
		  	 */
		  if (RxHeader.DLC != 8) {
			  return ;
		  }
		  int i = 0;

		  double temp_double[2];
		  temp_double[i++] = (int16_t)(( (uint16_t)RxData[3] << 8) | RxData[4]) / 100.0;
		  temp_double[i++] = (int16_t)(( (uint16_t)RxData[5] << 8) | RxData[6]) / 100.0;

		  uint8_t flags[4];
		  i = 0;
		  flags[i++] = (RxData[7] >> 6) & 0x3;
		  flags[i++] = (RxData[7] >> 4) & 0x3;
		  flags[i++] = (RxData[7] >> 2) & 0x3;
		  flags[i++] = RxData[7] & 0x3;

		  uint16_t lc = (RxData[0] << 8) | RxData[1];

		  uint8_t *temp;

		  if (chose == 0) {
			  temp = uart_buffer;
			  size1 += sprintf(
					  (char  *)(temp + size1),
					  "lc1:%.2f,lc2:0.0,lc3:0.0,pr:%.2f,pi:%.2f,mt:%d,mf:%d,mp:%d,mr:%d,",
					  (float)lc/100.0, temp_double[0], temp_double[1], flags[0], flags[1], flags[2], flags[3]
			  );
		  } else if (chose == 1) {
			  temp = uart_buffer2;
			  size2 += sprintf(
					  (char  *)(temp + size2),
					  "lc1:%.2f,lc2:0.0,lc3:0.0,pr:%.2f,pi:%.2f,mt:%d,mf:%d,mp:%d,mr:%d,",
					  (float)lc/100.0, temp_double[0], temp_double[1], flags[0], flags[1], flags[2], flags[3]
			  );
		  }

		  ++counter1;

		  break;
	  }
	  case CAN_TX_FRAME_2:
	  {
		  if (RxHeader.DLC != 8) {
			  return ;
		  }

		  double temp_double[2];
		  temp_double[0] = (int16_t)(( (uint16_t)RxData[0] << 8) | RxData[1])/ 100.0;
		  temp_double[1] = (int16_t)(( (uint16_t)RxData[2] << 8) | RxData[3])/ 100.0;

		  uint8_t flags[2];
		  flags[0] = (RxData[4] >> 4) & 0xff;
		  flags[1] = RxData[4] & 0xf;

		  progress = flags[0];
		  uint8_t *temp;

		  if (chose == 0) {
			  size1 += sprintf(
					  (char *)(uart_buffer + size1),
					  "pc:%.2f,lr:%.2f,p:%d,mq:%d,wp:%d,wt:%d,wf:%d,",
					  temp_double[0], temp_double[1], flags[0], flags[1], (RxData[5] << 4), (RxData[6] << 4), (RxData[7] << 4)
			  );
		  } else if (chose == 1) {
			  size2 += sprintf(
					  (char *)(uart_buffer2 + size2),
					  "pc:%.2f,lr:%.2f,p:%d,mq:%d,wp:%d,wt:%d,wf:%d,",
					  temp_double[0], temp_double[1], flags[0], flags[1], (RxData[5] << 4), (RxData[6] << 4), (RxData[7] << 4)
			  );
		  }
		  ++counter2;

		  break;
	  }
	  default:
		  break;
	  }

  }
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	switch(GPIO_Pin) {
	case GPIO_PIN_6:
//		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
		break;
	case GPIO_PIN_7:
		// Parachutes
		break;
	case GPIO_PIN_8:
		if (kg_counter == 0) {
			flags.kg_0 = 1;
			flags.kg_1_7 = 0;
			flags.kg_3_3 = 0;
			++kg_counter;
		}
		else if (kg_counter == 1) {
			flags.kg_0 = 0;
			flags.kg_1_7 = 1;
			flags.kg_3_3 = 0;
			++kg_counter;
		}
		else if (kg_counter == 2) {
			flags.kg_0 = 0;
			flags.kg_1_7 = 0;
			flags.kg_3_3 = 1;
			++kg_counter;
		}
		kg_counter %= 3;
		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);

		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
		break;
	case GPIO_PIN_9:
		flags.purgeActivated = 1;
		break;
	case GPIO_PIN_10:
		flags.launchActivated = 1;
		break;
	case GPIO_PIN_11:
		// DROGUE
		break;
	default:
		break;
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
